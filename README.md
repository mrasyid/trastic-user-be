# Trastic Backend API

## Table of Contents

- [Local Configuration](#local-configuration)
- [Deployed API URLs](#deployed-api-urls)

## Local Configuration Backend

- Install Python 3.6 or higher and PostgreSQL
- Create Python virtual environment
  ```bash
  $ cd /path/to/project/directory
  $ python3 -m venv env
  $ source env/bin/activate
  $ pip3 install -r requirements.txt
  ```
- Migrate the database
  ```bash
  $ python3 manage.py migrate
  ```
- Create Superuser
  ```bash
  $ python3 manage.py createsuperuser
  ```
- Run server
  ```bash
  $ python3 manage.py runserver
  ```

## Deployed API URLs

- Staging: [https://trastic.herokuapp.com/](https://trastic.herokuapp.com/)

