from django.contrib import admin

from .models import Transaction, ExchangeSatuan
admin.site.register(Transaction)
admin.site.register(ExchangeSatuan)