from django.db import models
from authentication.models import User, Pengguna, Warung, phoneModel
import random
from django.utils import timezone

    
TRANSACTION_STATUS_CHOICES = (
    ("Menunggu Konfirmasi", "Menunggu Konfirmasi"),
    ("Gagal", "Gagal"),
    ("Selesai", "Selesai")
)

class Transaction(models.Model):
    custom_id = models.CharField(max_length=255, primary_key=True)
    total_poin = models.IntegerField(default=0, blank=False)
    status = models.CharField(max_length=30, choices=TRANSACTION_STATUS_CHOICES, default=TRANSACTION_STATUS_CHOICES[0][0] ,blank=True)
    
    def __str__(self):
        return str(self.custom_id)
    
    def save(self,*args, **kwargs):
        if not self.custom_id:
            prefix = 'TRX{}'.format(timezone.now().strftime('%y%m%d'))
            prev_instances = self.__class__.objects.filter(custom_id__contains=prefix)
            if prev_instances.exists():
                last_instance_id = prev_instances.last().custom_id[-4:]
                self.custom_id = prefix+'{0:04d}'.format(int(last_instance_id)+1)
            else:
                self.custom_id = prefix+'{0:04d}'.format(1)
        super(Transaction, self).save(*args, **kwargs)
 

class ExchangeSatuan(models.Model):
    custom_id = models.CharField(max_length=255, primary_key=True)
    pengguna = models.ForeignKey(to=Pengguna, default=None, blank=True, null=True, on_delete=models.CASCADE)
    transaksi = models.ForeignKey(to=Transaction, default=None, blank=True, null=True, on_delete=models.CASCADE)
    warung = models.ForeignKey(to=Warung, null=True, default=None, blank=True, on_delete=models.CASCADE)
    botol_besar = models.IntegerField(default=0, blank=False)
    botol_sedang = models.IntegerField(default=0, blank=False) 
    botol_kecil = models.IntegerField(default=0, blank=False)
    gelas_besar = models.IntegerField(default=0, blank=False)
    gelas_kecil = models.IntegerField(default=0, blank=False)
    total_poin = models.IntegerField(default=0, blank=False)
    date = models.DateTimeField(auto_now_add=True,editable=False)
    is_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return str(self.custom_id)

    def save(self,*args, **kwargs):
        if not self.custom_id:
            prefix = 'S{}'.format(timezone.now().strftime('%y%m%d'))
            prev_instances = self.__class__.objects.filter(custom_id__contains=prefix)
            if prev_instances.exists():
                last_instance_id = prev_instances.last().custom_id[-4:]
                self.custom_id = prefix+'{0:04d}'.format(int(last_instance_id)+1)
            else:
                self.custom_id = prefix+'{0:04d}'.format(1)
        
        if self.is_confirmed:
            self.transaksi.status = "Selesai"
            self.transaksi.save()
            self.pengguna.poin += self.total_poin
            print("Poin Pengguna Ditambahakn")
            total_penukaran = self.botol_besar + self.botol_sedang + self.botol_kecil + self.gelas_besar + self.gelas_kecil
            self.pengguna.jumlahPenukaran += total_penukaran
            print("Total Penukaran Pengguna Ditambahakn")
            self.pengguna.save()
        super(ExchangeSatuan, self).save(*args, **kwargs)



