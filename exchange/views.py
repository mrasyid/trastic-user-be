from django.shortcuts import render

from http.client import NOT_FOUND
from django.shortcuts import render
import os
from authentication.models import User, Pengguna, Warung
from exchange.models import Transaction, ExchangeSatuan
from exchange.permissions import IsWarung
from rest_framework.views import APIView
from rest_framework import generics, status, views, permissions, viewsets
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, CreateAPIView
from .serializers import ExchangeSatuanSerializer
from django.http import HttpResponsePermanentRedirect
from rest_framework.response import Response
from django.urls import reverse
import jwt
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
import pyotp
from rest_framework.response import Response
from rest_framework.views import APIView


class ExchangeSatuanView(CreateAPIView):
    serializer_class = ExchangeSatuanSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        pengguna = Pengguna.objects.get(user=self.request.user)
        transaksi = Transaction.objects.create()
        warung = Warung.objects.get(custom_id=self.request.data["id_warung"])
        serializer_data =  serializer.save(pengguna=pengguna, transaksi= transaksi, warung=warung)
        transaksi_data = serializer.data
        transaksi.total_poin = transaksi_data["total_poin"]
        transaksi.save()
        return serializer_data

class UpdateExchangeSatuanView(UpdateAPIView):
    serializer_class = ExchangeSatuanSerializer
    permission_classes = (permissions.IsAuthenticated, IsWarung)
    lookup_field = "custom_id"
    queryset = ExchangeSatuan.objects.all()
