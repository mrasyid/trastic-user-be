from django.urls import path, re_path
from .views import ExchangeSatuanView, UpdateExchangeSatuanView

app_name = "exchange"
urlpatterns = [
    path('exchange-satuan/', ExchangeSatuanView.as_view(),name='exchange-satuan'),
    re_path('^exchange-satuan/update/(?P<custom_id>\w+)$',UpdateExchangeSatuanView.as_view(), name='update exchange satuan')
]