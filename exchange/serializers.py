from rest_framework import serializers
from .models import Transaction, ExchangeSatuan


class ExchangeSatuanSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExchangeSatuan
        fields = ["pengguna_id", "transaksi_id", "warung_id", "botol_besar", "botol_sedang", "botol_kecil", "gelas_besar", "gelas_kecil", "date", "total_poin","is_confirmed"]