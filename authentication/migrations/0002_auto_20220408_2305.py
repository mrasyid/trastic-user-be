# Generated by Django 3.1.7 on 2022-04-08 23:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonemodel',
            name='number',
            field=models.CharField(max_length=20, primary_key=True, serialize=False),
        ),
    ]
