from http.client import NOT_FOUND
from django.shortcuts import render
import os
from .models import User, Pengguna, phoneModel, Warung
from rest_framework.views import APIView
from rest_framework import generics, status, views, permissions, viewsets
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView
from .serializers import RegisterPenggunaSerializer, LoginSerializer, LogoutSerializer, ResetPasswordEmailRequestSerializer, SetNewPasswordSerializer, ChangePasswordSerializer, RegisterWarungSerializer
from django.http import HttpResponsePermanentRedirect
from rest_framework.response import Response
from django.urls import reverse
from rest_framework_simplejwt.tokens import RefreshToken
from .utils import Util
import jwt
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
import pyotp
from rest_framework.response import Response
from rest_framework.views import APIView

class CustomRedirect(HttpResponsePermanentRedirect):
    allowed_schemes = [os.environ.get('APP_SCHEME'), 'http', 'https']

# Time after which OTP will expire
EXPIRY_TIME = 50 # seconds

class RegisterPenggunaView(generics.GenericAPIView):
    serializer_class = RegisterPenggunaSerializer

    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user_data = serializer.data
        user = User.objects.get(email=user_data["user"]["email"])
        pengguna = Pengguna.objects.get(user=user)

        phone = phoneModel.objects.get(pengguna=pengguna)
        phone.counter += 1
        phone.save()
        
        key = pyotp.random_base32()
        phone.key = key
        phone.save()

        OTP = pyotp.HOTP(key)  # TOTP Model for OTP is created
    
        data = {
            'otp': OTP.at(phone.counter), 
            'pengguna': pengguna.name, 
            'target_email': user.email
        }
        print("OTP: "+ str(OTP.at(phone.counter)))
        print("Counter: "+ str(phone.counter))
        
        Util.send_email_pengguna(data)
        
        return Response(user_data, status=status.HTTP_201_CREATED)

class RegisterWarungView(generics.GenericAPIView):
    serializer_class = RegisterWarungSerializer

    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user_data = serializer.data
        user = User.objects.get(email=user_data["user"]["email"])
        return Response(user_data, status=status.HTTP_201_CREATED)

class GetPenggunaListView(ListAPIView):
    serializer_class = RegisterPenggunaSerializer

    def get_queryset(self):
        return Pengguna.objects.all()
    
class GetWarungListView(ListAPIView):
    serializer_class = RegisterWarungSerializer

    def get_queryset(self):
        return Warung.objects.all()
    
class GetPenggunaView(RetrieveAPIView):
    serializer_class = RegisterPenggunaSerializer

    def get_object(self):
        user_id = self.kwargs['id']
        try:
            pengguna = Pengguna.objects.get(user=User.objects.get(id=user_id))
            return pengguna
        
        except Pengguna.DoesNotExist:
            return Response({'message': 'User tidak ditemukan'}, status=status.HTTP_400_BAD_REQUEST)

class GetWarungByQRID(generics.GenericAPIView):
    def get(self, request):
        csid = request.GET.get('csid') 
        if Warung.objects.filter(custom_id=csid).exists():
            warung = Warung.objects.get(custom_id=csid)
            status_code = status.HTTP_200_OK
            response = {
                'success': 'true',
                'status code': status_code,
                'message': 'Detail warung ditemukan',
                'data': [{
                    'id': warung.custom_id,
                    'email': warung.user.email,
                    'nama': warung.name,
                    'city': warung.city,
                    'alamat': warung.alamat,
                    'poin': warung.poin
                }]
            }
            return Response(response)
        else:
            return Response({'message': "Warung tidak ditemukan"}, status=status.HTTP_400_BAD_REQUEST)
    
class VerifyEmail(generics.GenericAPIView):
    def get(self, request):
        phone = request.GET.get('phone')
        otp = request.GET.get('otp')
        
        try:
            phone_model = phoneModel.objects.get(number=phone)
        except ObjectDoesNotExist:
            return Response({"message": "Nomer HP tidak terdaftar"}, status=404)
        
        pengguna = Pengguna.objects.get(phone=phone_model)
        user = User.objects.get(pengguna=pengguna)
        
        if not user.is_verified:
            key = phone_model.key
            OTP = pyotp.HOTP(key)  # TOTP Model 
            
            if OTP.verify(otp, phone_model.counter) :  # Verifying the OTP
                phone_model.isVerified = True
                phone_model.counter += 1
                phone_model.save()
                user.is_verified = True
                user.save()
                return Response({'message': "Akun Anda berhasil diverifikasi"}, status=status.HTTP_200_OK)
            return Response({'message': "Kode OTP salah"}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'message': "Akun sudah pernah diaktivasi"}, status=status.HTTP_208_ALREADY_REPORTED)
        
class LoginAPIView(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
    
class LogoutAPIView(generics.GenericAPIView):
    serializer_class = LogoutSerializer

    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'message': 'Berhasil logout'},status=status.HTTP_200_OK)
    
class RequestPasswordResetEmail(generics.GenericAPIView):
    serializer_class = ResetPasswordEmailRequestSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)

        email = request.data.get('email', '')
        print(email)
        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            pengguna = Pengguna.objects.get(user=user)

            phone = phoneModel.objects.get(pengguna=pengguna)
            phone.counter += 1
            phone.save()

            key = pyotp.random_base32()
            phone.key = key
            phone.save()

            OTP = pyotp.HOTP(key)  # TOTP Model for OTP is created
        
            data = {
                'otp': OTP.at(phone.counter), 
                'pengguna': pengguna.name, 
                'target_email': user.email
            }
            
            Util.send_email_forget_password(data)
            return Response({
                'message': 'Kode OTP sudah dikirimkan',
            }, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'Akun tidak ditemukan'}, status=status.HTTP_400_BAD_REQUEST)
        
class OTPCheckAPI(generics.GenericAPIView):
    def get(self, request, email, otp):
        user = User.objects.get(email=email)
        pengguna = Pengguna.objects.get(user=user)
        
        phone_model = phoneModel.objects.get(pengguna=pengguna)
        key = phone_model.key
        OTP = pyotp.HOTP(key)  # TOTP Model 
        
        if OTP.verify(otp, phone_model.counter) :  # Verifying the OTP
            phone_model.counter += 1
            phone_model.save()
            return Response({'message': "OTP Benar"}, status=status.HTTP_200_OK)
        return Response({'message': "OTP Salah, silahkan coba lagi"}, status=status.HTTP_400_BAD_REQUEST)
    
class SetNewPasswordAPIView(generics.GenericAPIView):
    serializer_class = SetNewPasswordSerializer

    def patch(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'message': 'Kata sandi berhasil diganti'}, status=status.HTTP_200_OK)
    
class GetUserByToken(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    def post(self, request): 
        try:    
            user = self.request.user
            if user.is_pengguna:
                pengguna = Pengguna.objects.get(user=user)
                status_code = status.HTTP_200_OK
                response = {
                    'success': 'true',
                    'status code': status_code,
                    'message': 'Detail pengguna ditemukan',
                    'data': [{
                        'email': user.email,
                        'nama': pengguna.name,
                        'city': pengguna.city,
                        'jumlahPenukaran': pengguna.jumlahPenukaran,
                        'pencapaian': pengguna.pencapaian,
                        'poin': pengguna.poin
                    }]
                }
                return Response(response)
        except Exception as e:
            status_code = status.HTTP_400_BAD_REQUEST
            response = {
                'success': 'false',
                'status code': status.HTTP_400_BAD_REQUEST,
                'message': 'Pengguna tidak dapat ditemukan',
                'error': str(e)
            }
            return Response(response)
            
class ChangePassword(generics.GenericAPIView):
    serializer_class = ChangePasswordSerializer
    permission_classes = (permissions.IsAuthenticated,)
    
    
    def patch(self, request):
        try:
            user = self.request.user
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid(raise_exception=True):
                if not user.check_password(serializer.data.get('old_password')):
                    return Response({'message': "Kata sandi salah"}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    user.set_password(serializer.data.get('new_password'))
                    user.save()
                    return Response({'message': 'Kata sandi berhasil diganti'}, status=status.HTTP_200_OK)
        except Exception as e:
            status_code = status.HTTP_401_UNAUTHORIZED
            response = {
                'message': str(e)
            }
            return Response(response)