from django.urls import path, re_path
from .views import GetWarungByQRID, RegisterPenggunaView,RegisterWarungView, GetPenggunaListView, VerifyEmail, LoginAPIView, GetPenggunaView, LogoutAPIView, RequestPasswordResetEmail, OTPCheckAPI, SetNewPasswordAPIView, GetUserByToken, ChangePassword, GetWarungByQRID

from rest_framework_simplejwt.views import (
    TokenRefreshView,
)

app_name = "authentication"
urlpatterns = [
    path('register-pengguna/', RegisterPenggunaView.as_view(),name='register-pengguna'),
    path('register-warung/', RegisterWarungView.as_view(),name='register-warung'),
    path('list-pengguna/', GetPenggunaListView.as_view(), name='list_pengguna'),
    re_path('^pengguna/(?P<id>\w+)$',GetPenggunaView.as_view(), name='get_pengguna'),
    path('verifikasi-email/', VerifyEmail.as_view(), name='verify'),
    path('login/', LoginAPIView.as_view(), name='login'),
    path('logout/', LogoutAPIView.as_view(), name='logout'),
    path('request-reset-email/', RequestPasswordResetEmail.as_view(),name="request-reset-email"),
    path('verify-otp/<email>/<otp>',OTPCheckAPI.as_view(), name='verify-otp'),
    path('set-new-password/', SetNewPasswordAPIView.as_view(),name='set-new-password'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('user-detail/', GetUserByToken.as_view(), name='user-detail'),
    path('warung/', GetWarungByQRID.as_view(), name='warung-detail'),
    path('change-password/', ChangePassword.as_view(), name='change-password'),
]