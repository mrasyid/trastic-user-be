from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt import token_blacklist
import qrcode
from django.core.files import File
import PIL
from io import StringIO, BytesIO

# Create your models here.
def upload_path_warung_img(instance, filename):
    extension = filename.split(".")[-1]
    upload_path =  "/".join(["warung", str(instance.custom_id) ,"{}-qr_code.{}".format(str(instance.custom_id), str(extension))])
    return upload_path

class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        if email is None:
            raise TypeError('Users should have an email')
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            is_active=True,
            is_staff=is_staff,
            is_superuser=is_superuser,
            **extra_fields
        )
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        user = self._create_user(email, password, True, True, **extra_fields)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True, db_index=True)
    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_pengguna = models.BooleanField(default=False)
    is_warung = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = "email"
    EMAIL_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            "refresh": str(refresh),
            "access": str(refresh.access_token)
        }

def upload_pengguna_profile_path(instance, filename):
    return "/".join(["pengguna_profile", str(instance.user), filename])

class Pengguna(models.Model):
    PENCAPAIAN_CHOICES = (
        ("level_0", "none"),
        ("level_1", "Pendaur Pemula"),
        ("level_2", "Pemerhati Daur Ulang"),
        ("level_3", "Duta Daur Ulang"),
        ("level_4", "Penyelamat Bumi"),
    )
    
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length=150)
    phone = models.CharField(max_length=30)
    profile_img = models.ImageField(
        blank=True, null=True, upload_to=upload_pengguna_profile_path)
    city = models.CharField(max_length=30, default="Bekasi")
    jumlahPenukaran = models.IntegerField(default=0)
    pencapaian = models.CharField(max_length=30, choices=PENCAPAIAN_CHOICES, default=PENCAPAIAN_CHOICES[0][0] ,blank=True) 
    poin = models.IntegerField(default =0)
    
    def __str__(self):
        return self.name

class Warung(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True)
    custom_id = models.CharField(max_length=7, null=True, default=None)
    name = models.CharField(max_length=150)
    phone = models.CharField(max_length=30)
    city = models.CharField(max_length=30, default="Bekasi")
    alamat = models.CharField(max_length=150)
    poin = models.IntegerField(default =0)
    qr_code = models.ImageField(blank=True, null=True, upload_to=upload_path_warung_img)

    
    def __str__(self):
        return self.name
    
    def save(self,*args, **kwargs):
       if not self.custom_id:
            if self.city.lower() == "bekasi":
                prefix = "BKS"
            elif self.city.lower() == "depok":
                prefix = "DPK"
            else:
                prefix = "JKT"
            prev_instances = self.__class__.objects.filter(custom_id__contains=prefix)
            if prev_instances.exists():
                last_instance_id = prev_instances.last().custom_id[-3:]
                self.custom_id = prefix+'{0:04d}'.format(int(last_instance_id)+1)
            else:
                self.custom_id = prefix+'{0:04d}'.format(1)
                
            qrcode_img = qrcode.make(self.custom_id) 
            canvas = PIL.Image.new('RGB', (290, 290), 'white')
            canvas.paste(qrcode_img)
            fname = f'qr_code-{self.custom_id}.png'
            buffer = BytesIO()
            canvas.save(buffer,'PNG')
            self.qr_code.save(fname, File(buffer), save=False)
            canvas.close()
        
       super(Warung, self).save(*args, **kwargs)
    
class phoneModel(models.Model):
    pengguna = models.OneToOneField(Pengguna, default=None, blank=True, null=True, on_delete=models.CASCADE)
    warung = models.OneToOneField(Warung, null=True, default=None, blank=True, on_delete=models.SET_NULL)
    number = models.CharField(blank=False, primary_key=True, max_length=20)
    isVerified = models.BooleanField(blank=False, default=False)
    counter = models.IntegerField(default=0, blank=False)   # For HOTP Verification
    key = models.CharField(max_length=150)

    def __str__(self):
        return str(self.number)